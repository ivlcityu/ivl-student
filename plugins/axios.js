const _get = require('lodash/get')
const _isString = require('lodash/isString')
const _isArray = require('lodash/isArray')
const _uniq = require('lodash/uniq')
const _map = require('lodash/map')
const _isObject = require('lodash/isObject')

function extractMsg(error) {
  const data = _get(error, 'response.data')
  if (_isString(data)) {
    return data
  }
  if (_isArray(data)) {
    return _uniq(_map(data, 'message')).join('\n')
  }
  if (_isObject(data)) {
    return _get(data, 'message', message)
  }
  return error.response.statusText
}

export default function({ store, $axios, redirect }) {
  $axios.onError((error) => {
    if (
      error.response.status === 400 ||
      error.response.status === 401 ||
      error.response.status >= 500
    ) {
      store.commit('errorMessage', extractMsg(error))
    }
  })
}
