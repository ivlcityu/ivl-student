export const state = () => ({
  errorMessage: ''
})

export const mutations = {
  errorMessage(state, message) {
    state.errorMessage = message
  }
}
